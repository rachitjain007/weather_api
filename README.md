# Weather API with Django

This project consists of four endpoints:-

1. `/api/weather/`
2. `/api/weather/stats/`
3. `/docs`
4. `/redoc`

# Project setup and installation

1. Clone the git repository.
2. Migrate to the project directory using the following command:-

```bash
    cd weather_API
```

3. Create and activate a virtual environment with the following commands:-

```bash
    python -m venv env
```

```bash
    source env/bin/activate # (For Mac and Linux)
    env\Scripts\activate    # (For Windows)
```

4. Install the requirements by executing the following command:-

```bash
    pip install -r requirements.txt
```

# Set up database

1. Download and install PostgreSQL using the link `https://www.postgresql.org/download/`
2. Create a database in PostgreSQL using below command:-

```bash
    CREATE DATABASE database_name;
```

# Configure environment variables

1. Create a `.env` file and store your database credentials in below format:-

```bash
    DB_NAME = 'name_of_db'
    DB_USER = 'username_of_db'
    DB_PASSWORD = 'password_of_db'
    DB_HOST = 'host_of_db'
    DB_PORT =  'port_no_of_db'
    TEST_DB_NAME = 'name_of_db'
    TEST_DB_USER = 'username_of_db'
    TEST_DB_PASSWORD = 'password_of_db'
    TEST_DB_HOST = 'host_of_db'
    TEST_DB_PORT =  'port_no_of_db'
```

# Make database migrations

1. Migrate to the directory `weather_API`
2. Make and apply migrations to the database using the following commands:-

```bash
    python manage.py makemigrations
    python manage.py migrate
```

# Ingest data into database

1. Ingest data into database using command:-

```bash
    python manage.py ingest data
```

# Run project

1. Migrate to the directory `weather_API`
2. Run the below command to load the data and run the project:-

```bash
    python manage.py runserver
```

<img src="./static/runtime.png" alt="runserver ouput" height="100" width="450"/>

##### This step will take few minutes as it will dump the data in the initial setup

# To access the API endpoints:

```bash
    /api/weather/           # for weather records
    /api/weather/stats/     # for weather stats
    /docs                   # for swagger documentation
    /redoc                  # for API documentation
```

# Testing

1. Create Testing Database

```bash
    CREATE DATABASE database_name
```

2. Run migrations for testing database

```bash
    python manage.py migrate --database=test
```

3. Run the testcases using the following command:-

```bash
    python manage.py test
```

<img src="./static/testcase.png" alt="logs" />

# Test coverage report

1. Check the test coverage using the following command:-

```bash
    coverage run manage.py test
```

2. Create report

```bash
    coverage report
```

<img src="./static/coverage-report.png" alt="logs" />

# Screenshot of Logs

<img src="./static/logs.png" alt="logs" />

# Screenshots of Postman collection

<img src="./static/api-weather.png" alt="postman-img 1" />
<br><br>
<img src="./static/api-weather-stats.png" alt="postman-img 2" />

# Screenshot of API Docs

<img src="./static/swagger.png" alt="Swagger image" />
