from django.contrib import admin
from .models import Record, Stats
# Register your models here.

@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'max_temperature', 'min_temperature', 'precipitation', 'station')


@admin.register(Stats)
class StatsAdmin(admin.ModelAdmin):
    list_display = ('id', 'avg_max_temperature', 'avg_min_temperature', 'total_precipitation', 'year', 'station')