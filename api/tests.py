from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from api.models import *
from api.data_ingestion import run_ingestion


class WeatherAPITestCase(TestCase):

    client = APIClient()

    def setUp(self):
        """
        Runs data ingestion for the test-data directory.
        """
        super().setUpClass()

        run_ingestion('api/test-data')

    def test_get_record_with_no_filter(self):
        """
        Test retrieving records without any filters.
        """
        response = self.client.get(reverse('router:record-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['results']), 20)

    def test_valid_get_record_response_with_station_filter(self):
        """
        Test retrieving records with a station filter.
        """
        response = self.client.get(
            reverse('router:record-list') + '?station=USC00110187')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()['results'][0]['station'], 'USC00110187')

    def test_invalid_get_record_response_with_station_filter(self):
        """
        Test retrieving records with a station filter.
        """
        response = self.client.get(
            reverse('router:record-list') + '?station=INVALID_ID')
        self.assertEqual(response.status_code, 404)

    def test_valid_get_record_response_with_date_filter(self):
        """
        Test retrieving records with a date filter.
        """
        response = self.client.get(
            reverse('router:record-list') + '?date=1985-01-01')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['results'][0]['date'], '1985-01-01')

    def test_invalid_get_record_response_with_date_filter(self):
        """
        Test retrieving records with a date filter.
        """
        response = self.client.get(
            reverse('router:record-list') + '?date=2069-01-01')
        self.assertEqual(response.status_code, 404)

    def test_get_record_response_with_both_filters(self):
        """
        Test retrieving records with both station and date filters.
        """
        response = self.client.get(
            reverse('router:record-list') + '?station=USC00110187&date=1985-01-01')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['results'][0]['date'], '1985-01-01')
        self.assertEqual(
            response.json()['results'][0]['station'], 'USC00110187')

    def test_get_record_response_pagination(self):
        """
        Test paginated retrieval of records.
        """
        response = self.client.get(
            reverse('router:record-list') + '?page=1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['results']), 20)

    def test_get_stats_without_filter(self):
        """
        Test retrieving statistics without any filters.
        """
        response = self.client.get(reverse('router:stats-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['results']), 2)

    def test_valid_get_stats_response_with_station_filter(self):
        """
        Test retrieving statistics with a station filter.
        """
        response = self.client.get(
            reverse('router:stats-list') + '?station=USC00110187')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['results']), 1)
        self.assertEqual(
            response.json()['results'][0]['station'], 'USC00110187')

    def test_invalid_get_stats_response_with_station_filter(self):
        """
        Test retrieving statistics with a station filter.
        """
        response = self.client.get(
            reverse('router:stats-list') + '?station=INVALID_ID')
        self.assertEqual(response.status_code, 404)

    def test_valid_all_stats_response_with_year_filter(self):
        """
        Test retrieving statistics with a year filter.
        """
        response = self.client.get(reverse('router:stats-list') + '?year=1985')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['results']), 2)
        self.assertEqual(response.json()['results'][0]['year'], 1985)

    def test_invalid_all_stats_response_with_year_filter(self):
        """
        Test retrieving statistics with a year filter.
        """
        response = self.client.get(reverse('router:stats-list') + '?year=2069')
        self.assertEqual(response.status_code, 404)

    def test_all_stats_response_with_both_filters(self):
        """
        Test retrieving statistics with both year and station filters.
        """
        response = self.client.get(
            reverse('router:stats-list') + '?year=1985&station=USC00110187')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['results']), 1)
        self.assertEqual(response.json()['results'][0]['year'], 1985)
        self.assertEqual(
            response.json()['results'][0]['station'], 'USC00110187')

    def test_all_stats_response_pagination(self):
        """
        Test paginated retrieval of statistics.
        """
        response = self.client.get(
            reverse('router:stats-list') + '?page=1')
        self.assertEqual(len(response.json()['results']), 2)
