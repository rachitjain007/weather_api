from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action

from .models import Record, Stats
from .serializers import RecordSerializer, StatsSerializer


class RecordViewSet(GenericViewSet):
    """
    API endpoint that allows viewing records. Retrieves filtered records using station and date. Returns paginated response.
    """
    queryset = Record.objects.all()
    serializer_class = RecordSerializer
    pagination_class = PageNumberPagination

    def list(self, request):
        try:
            queryset = self.queryset
            station = request.GET.get('station')
            date = request.GET.get('date')
            if station:
                queryset = queryset.filter(station=station)
                if not queryset.exists():
                    return Response(f'Station {station} not found.', status=status.HTTP_404_NOT_FOUND)
            if date:
                queryset = queryset.filter(date=date)
                if not queryset.exists():
                    return Response(f'Date {date} not found.', status=status.HTTP_404_NOT_FOUND)

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'message': 'Error occured', 'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class StatsViewSet(GenericViewSet):
    """
    API endpoint that allows viewing statistics. Retrieves filtered using station and year. Returns paginated response.
    """
    queryset = Stats.objects.all()
    serializer_class = StatsSerializer
    pagination_class = PageNumberPagination

    def list(self, request):
        try:
            queryset = self.queryset
            station = request.GET.get('station')
            year = request.GET.get('year')
            if station:
                queryset = queryset.filter(station=station)
                if not queryset.exists():
                    return Response(f'Station {station} not found.', status=status.HTTP_404_NOT_FOUND)
            if year:
                queryset = queryset.filter(year=int(year))
                if not queryset.exists():
                    return Response(f'Year {year} not found.', status=status.HTTP_404_NOT_FOUND)

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'message': 'Error occured', 'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
