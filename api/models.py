from django.db import models

# Create your models here.


class Record(models.Model):
    date = models.DateField()
    max_temperature = models.IntegerField(default=0, null=True, blank=True)
    min_temperature = models.IntegerField(default=0, null=True, blank=True)
    precipitation = models.IntegerField(default=0, null=True, blank=True)
    station = models.CharField(max_length=50)

    class Meta:
        unique_together = ('date', 'station')
        ordering = ('id',)


class Stats(models.Model):
    avg_max_temperature = models.FloatField(null=True, blank=True)
    avg_min_temperature = models.FloatField(null=True, blank=True)
    total_precipitation = models.FloatField(null=True, blank=True)
    year = models.IntegerField()
    station = models.CharField(max_length=50)

    class Meta:
        unique_together = ('year', 'station')
        ordering = ('id',)
