from django.core.management import BaseCommand
from api.data_ingestion import run_ingestion


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    help = "Ingests data from data/wx_data"

    def handle(self, *args, **options):
        # Implement the logic here
        run_ingestion()
