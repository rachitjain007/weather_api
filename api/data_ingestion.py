import sys
import os
import logging
from datetime import datetime
from django.db.models import Avg, Sum, F
from .models import *
from django.db.models import Avg, Sum, F, ExpressionWrapper, DecimalField


def get_file_data(path):
    """
    Read and extract data from the file at the given path.
    """
    try:

        with open(path, 'r') as file:
            file_data = file.read().strip(' ').strip('\n').split('\n')
            return file_data

    except Exception as e:
        return [str(e)]


def construct_weather_data(file, data):
    """
    Construct and insert weather data into the Record model.
    """
    try:
        single_reading = [x.strip(" ") for x in data.split('\t')]
        Record.objects.create(
            date=None if single_reading[0] == '' or single_reading[0] == '-9999' else datetime.strptime(
                single_reading[0], '%Y%m%d'),
            max_temperature=None if single_reading[1] == '' or single_reading[1] == '-9999' else int(
                single_reading[1]),
            min_temperature=None if single_reading[2] == '' or single_reading[2] == '-9999' else int(
                single_reading[2]),
            precipitation=None if single_reading[3] == '' or single_reading[3] == '-9999' else int(
                single_reading[3]),
            station=file.replace('.txt', '')
        )
    except Exception as e:
        print(str(e))


def insert_file_data(path):
    """
    Ingest weather data from files into the Record model.
    """
    try:
        path = os.path.join(os.getcwd(), path)
        files = os.listdir(path)
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S',
                            handlers=[
                                logging.FileHandler('logs.log')
                            ]
                            )
        log_message = f'Record ingestion started at {datetime.now()}'
        logging.info(log_message)
        for file in files:
            if file[:3] != 'USC':
                continue
            file_data = get_file_data(os.path.join(path, file))
            if not file_data:
                continue
            for data in file_data:
                if len(data) == 0:
                    continue
                construct_weather_data(file, data)

        log_message = f'Record ingestion finished at {datetime.now()}. Total records ingested: {Record.objects.all().count()}'
        logging.info(log_message)
    except Exception as e:
        logging.error(str(e))
        print(str(e))
        sys.exit()


def calculate_stats():
    """
    Calculate and insert statistics from the Record model into the Stats model.
    """
    try:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S',
                            handlers=[
                                logging.FileHandler('logs.log')
                            ]
                            )
        log_message = f'Stats ingestion started at {datetime.now()}'
        queryset = Record.objects.values('date__year', 'station').annotate(
            avg_max_temperature=ExpressionWrapper(
                Avg(F('max_temperature') / 10), output_field=DecimalField()),
            avg_min_temperature=ExpressionWrapper(
                Avg(F('min_temperature') / 10), output_field=DecimalField()),
            total_precipitation=ExpressionWrapper(
                Sum(F('precipitation') / 100.0), output_field=DecimalField())
        ).order_by('date__year', 'station')

        for row in queryset:
            Stats.objects.create(
                avg_max_temperature=row.get('avg_max_temperature'),
                avg_min_temperature=row.get('avg_min_temperature'),
                total_precipitation=row.get('total_precipitation'),
                year=row.get('date__year'),
                station=row.get('station')
            )

        log_message = f'Stats ingestion finished at {datetime.now()}. Total stats ingested: {Stats.objects.all().count()}'
        logging.info(log_message)
    except Exception as e:
        print(str(e))
        logging.error(str(e))


def run_ingestion(path='data/wx_data'):
    """
    Run the data ingestion process.
    """
    insert_file_data(path)
    calculate_stats()
